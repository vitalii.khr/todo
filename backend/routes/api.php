<?php

use App\Services\Todos\Http\Controllers\TodoController;
use App\Services\Todos\Http\Controllers\TodoItemController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'todo',
], function () {
    Route::get('/', [TodoController::class, 'index']);
    Route::post('/', [TodoController::class, 'create']);
    Route::get('/completed', [TodoController::class, 'getCompleted']);
    Route::get('/{id}', [TodoItemController::class, 'get']);
    Route::put('/{id}/complete', [TodoItemController::class, 'complete']);
});

