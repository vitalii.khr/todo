<?php

namespace App\Services\Todos\Models;

use Database\Factories\ItemFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table = 'todo_items';

    protected $fillable = [
        'title',
        'content',
        'completed_at',
    ];

    protected $casts = [
        'completed_at' => 'immutable_datetime',
    ];

    public function complete(): void
    {
        if ($this->completed_at) {
            throw new \RuntimeException('TODO item is already completed.');
        }

        $this->completed_at = now();
    }

    protected static function newFactory()
    {
        return new ItemFactory();
    }
}
