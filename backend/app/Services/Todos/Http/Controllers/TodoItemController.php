<?php

namespace App\Services\Todos\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Todos\Models\Item;
use App\Services\Todos\Resources\ItemResource;

class TodoItemController extends Controller
{
    public function get(string $id): ItemResource
    {
        return new ItemResource(
            Item::findOrFail($id)
        );
    }

    public function complete(string $id): ItemResource
    {
        $item = Item::findOrFail($id);
        $item->complete();
        $item->save();

        return new ItemResource(
            $item->refresh()
        );
    }
}
