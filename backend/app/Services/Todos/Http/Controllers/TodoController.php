<?php

namespace App\Services\Todos\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Todos\Http\Requests\CreateItemRequest;
use App\Services\Todos\Models\Item;
use App\Services\Todos\Resources\ItemCollection;
use App\Services\Todos\Resources\ItemResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TodoController extends Controller
{
    public function index(): ResourceCollection
    {
        $builder = Item::query()->orderBy('title');

        return new ItemCollection(
            $builder->get()
        );
    }

    public function create(CreateItemRequest $request): JsonResource
    {
        $item = new Item();
        $item->title = $request->get('title');
        $item->content = $request->get('content');
        $item->save();

        return new ItemResource(
            $item->refresh()
        );
    }

    public function getCompleted(): ResourceCollection
    {
        $builder = Item::query()
            ->whereNotNull('completed_at')
            ->orderBy('title');

        return new ItemCollection(
            $builder->get()
        );
    }
}
