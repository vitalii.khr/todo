<?php

namespace App\Services\Todos\Resources;

use App\Services\Todos\Models\Item;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * @var Item
     */
    public $resource;

    public static $wrap = null;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'title' => $this->resource->title,
            'content' => $this->resource->content,
            'completed_at' => $this->resource->completed_at,
            'created_at' => $this->resource->created_at,
        ];
    }
}
