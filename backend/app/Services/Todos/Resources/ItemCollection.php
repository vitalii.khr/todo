<?php

namespace App\Services\Todos\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemCollection extends ResourceCollection
{
    public static $wrap = null;

    public $collects = ItemResource::class;
}
