<?php

namespace Database\Factories;

use App\Services\Todos\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * @inheritdoc
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(),
            'completed_at' => null,
        ];
    }

    public function completed()
    {
        return $this->state(function (array $attributes) {
            return [
                'completed_at' => now()
            ];
        });
    }

}
