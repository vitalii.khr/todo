<?php

namespace Tests\Feature\Services\Todo\Http\Controllers;

use App\Services\Todos\Models\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\ResourceStructures;

class TodoControllerTest extends TestCase
{
    use RefreshDatabase;
    use ResourceStructures;

    public function test_index_should_return_ok_and_items(): void
    {
        Item::factory()->count(7)->create();
        Item::factory()->completed()->count(7)->create();

        $response = $this->getJson('/api/todo');

        $response
            ->assertOk()
            ->assertJsonStructure([$this->todoItemResourceStructure()])
            ->assertJsonCount(14);
    }

    public function test_get_completed_should_return_ok_and_items(): void
    {
        $completedCount = 10;

        Item::factory()->count(7)->create();
        Item::factory()->completed()->count($completedCount)->create();

        $response = $this->getJson('/api/todo/completed');

        $response
            ->assertOk()
            ->assertJsonStructure([$this->todoItemResourceStructure()])
            ->assertJsonCount($completedCount);
    }

    public function test_create_item_should_return_created_and_item(): void
    {
        $data = [
            'title' => 'Make this thing as soon as possible.',
            'content' => 'It should be done because you need it. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        ];

        $response = $this->postJson('/api/todo', $data);

        $response
            ->assertCreated()
            ->assertJsonStructure($this->todoItemResourceStructure())
            ->assertJsonFragment($data);
    }

    public function test_create_item_using_invalid_title_should_return_error(): void
    {
        $data = [
            'title' => null,
            'content' => 'It should be done because you need it. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        ];

        $response = $this->postJson('/api/todo', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrorFor('title');
    }

    public function test_create_item_using_invalid_content_should_return_error(): void
    {
        $data = [
            'title' => 'It should be done because you need it.',
            'content' => null,
        ];

        $response = $this->postJson('/api/todo', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrorFor('content');
    }

    public function test_create_item_using_invalid_data_should_return_errors(): void
    {
        $data = [
            'title' => null,
            'content' => null,
        ];

        $response = $this->postJson('/api/todo', $data);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrorFor('title')
            ->assertJsonValidationErrorFor('content');
    }
}
