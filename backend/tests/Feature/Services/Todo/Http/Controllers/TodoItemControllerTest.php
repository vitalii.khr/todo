<?php

namespace Tests\Feature\Services\Todo\Http\Controllers;

use App\Services\Todos\Models\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Tests\TestCase;
use Tests\Traits\ResourceStructures;

class TodoItemControllerTest extends TestCase
{
    use RefreshDatabase;
    use ResourceStructures;

    public function test_get_item_should_return_ok_and_item()
    {
        Item::factory()->count(7)->create();

        $title = 'Title for exact checking';
        $content = 'Content for exact checking';

        $item = Item::factory()->completed()->create([
            'title' => $title,
            'content' => $content,
        ]);

        $response = $this->getJson('/api/todo/'.$item->id);

        $response
            ->assertOk()
            ->assertJsonStructure($this->todoItemResourceStructure())
            ->assertJsonFragment([
                'id' => $item->id,
                'title' => $title,
                'content' => $content,
            ]);
    }

    public function test_get_invalid_item_should_return_not_found(): void
    {
        Item::factory()->count(7)->create();

        $this
            ->getJson('/api/todo/'.Str::random())
            ->assertNotFound();
    }

    public function test_complete_item_should_return_ok_and_item(): void
    {
        $item = Item::factory()->create([
            'title' => 'Title for checking',
            'content' => 'Content for checking',
        ]);

        $response = $this->putJson('/api/todo/'.$item->id.'/complete');

        $response
            ->assertOk()
            ->assertJsonStructure($this->todoItemResourceStructure());

        $this->assertEquals(
            1,
            DB::table((new Item())->getTable())
                ->where('id', $item->id)
                ->whereNotNull('completed_at')
                ->count()
        );
    }

    public function test_complete_already_completed_item_should_return_error(): void
    {
        $item = Item::factory()->create([
            'title' => 'Title for checking',
            'content' => 'Content for checking',
            'completed_at' => now(),
        ]);

        $this
            ->putJson('/api/todo/'.$item->id.'/complete')
            ->assertStatus(500);
    }

    public function test_complete_invalid_item_should_return_not_found(): void
    {
        $this
            ->putJson('/api/todo/'.Str::random().'/complete')
            ->assertNotFound();
    }
}
