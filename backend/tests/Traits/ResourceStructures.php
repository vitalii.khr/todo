<?php

namespace Tests\Traits;

trait ResourceStructures
{
    public function todoItemResourceStructure(): array
    {
        return [
            'id',
            'title',
            'content',
            'completed_at',
            'created_at',
        ];
    }
}
